<?php

namespace console\controllers;

use yii\console\Controller;
use console\models\Log;
use Yii;

/**
 * @author foxit
 */
class LogController extends Controller {

    /**
     * @param string $text
     */
    public function actionTime($text = NULL) {

        Log::getTime($text);
    }
}
