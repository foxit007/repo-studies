<?php

namespace console\controllers;

use yii\console\Controller;
use console\models\News;
use console\models\Subscriber;
use console\models\Sender;
use console\models\Worker;
use Yii;

/**
 * @author foxit
 */
class MailerController extends Controller {

    /**
     * @param class $class
     */
    public function actionSend($class) {
        if ($class == 'News') {
            $listNews = News::getList();
            $subsriber = Subscriber::getList();
            Sender::run($subsriber, $listNews, 'Рассылка новостей', 'News');
        }

        if ($class == 'Worker') {
            $solaryList = Worker::getSalary();
            Sender::run('', $solaryList, 'Начисление зарплаты', 'Worker');
        }
    }

}
