<?php

namespace console\models;

use Yii;

/**
 * @author foxit
 */
class Log {

    /**
     * @param string $text
     */
    public static function getTime($text) {
        $time = Yii::$app->formatter->asTime(time()) .
                "\x20" .
                Yii::$app->formatter->asDate(time()) .
                $text . "\n";

        $handle = fopen("/var/www/project/frontend/web/log.txt", "a+b");
        fwrite($handle, $time);
        fclose($handle);
    }

}
