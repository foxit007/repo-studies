<?php

namespace console\models;
use Yii;
/**
 * @author foxit
 */
class Worker {
    
    /**
     * @return array
     */
    public static function getSalary(){
        
        $sql='SELECT * FROM worker';
        return Yii::$app->db->createCommand($sql)->queryAll();
    }
}
