<?php

namespace console\models;

use Yii;

/**
 * @author foxit
 */
class Sender {

    /**
     * @param string $subsriber
     * @param string $list
     * @param string $subject
     * @param class $class
     */
    public static function run($subscriber, $list, $subject, $class) {
        if ($class == 'News') {
            foreach ($subscriber as $subscribers) {
                $result = Yii::$app->mailer->compose('/mailer/listMailer', [
                            'list' => $list,
                            'class' => $class,
                        ])
                        ->setFrom('rotokan2@gmail.com')
                        ->setTo($subscribers['email'])
                        ->setSubject($subject)
                        ->send();
                if ($result) {
                    Log::getTime(' Новости на электронную почту ' . 
                            $subscribers['email'].
                            ' отправленны успешно');
                } else {
                    Log::getTime(' Новости на электронную почту '.
                            $subscribers['email'].
                            ' не отправленны');
                }
            }
        }
        if ($class == 'Worker') {
            foreach ($list as $item) {
                $result = Yii::$app->mailer->compose('/mailer/listMailer', [
                            'list' => $list,
                            'class' => $class,
                            'email' => $item['email'],
                        ])
                        ->setFrom('rotokan2@gmail.com')
                        ->setTo($item['email'])
                        ->setSubject($subject)
                        ->send();
                if ($result) {
                    Log::getTime(' Данные о начислении заработной платы на электронную почту '.
                            $item['email'].
                            ' отправленны успешно');
                } else {
                    Log::getTime(' Данные о начислении заработной платы на электронную почту '.
                            $item['email'].
                            ' не отправленны');
                }
            }
        }
    }

}
