<?php

namespace console\models;

use Yii;

/**
 * @author foxit
 */
class News {

    const STATUS_NOT_SEND = 1;

    /**
     * @return array type
     */
    public static function getList() {

        $sql = 'SELECT * FROM news WHERE status = ' . self::STATUS_NOT_SEND; //magic number
        $result = Yii::$app->db->createCommand($sql)->queryAll();
        return self::prepareList($result);
    }

    /**
     * @param array $result
     * @return array|srting
     */
    public static function prepareList($result) {
        if (!empty($result) && is_array($result)) {
            foreach ($result as &$item) {
                $item['content'] = Yii::$app->stringHelper->getShortByWord($item['content']);
            }
        }
        return $result;
    }

}
