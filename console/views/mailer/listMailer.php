<?php if($class=='News'): ?>
    <?php foreach($list as $item): ?>
        <h1><?php echo $item['title']; ?></h1>
        <p><?php echo $item['content']; ?></p>
        <a href="<?php echo $item['link']; ?>" 
            style="text-decoration:none; 
           text-align:center; 
           padding:5px 6px; 
           border:solid 2px #004F72; 
           -webkit-border-radius:18px;
           -moz-border-radius:18px; 
           border-radius: 18px; 
           font:13px Arial, Helvetica, sans-serif; 
           font-weight:bold; 
           color:#E5FFFF; 
           background-color:#3BA4C7; 
           background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); 
           background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); 
           background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); 
           background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); 
           filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); 
           background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%);   
           -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; 
           -moz-box-shadow: 0px 0px 2px #bababa,  inset 0px 0px 1px #ffffff;  
           box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff;  
           text-shadow: -5px -5px 5px #bababa; 
           filter: dropshadow(color=#bababa, offx=-5, offy=-5);">
            Перейти
        </a>
        <br><br><hr>
    <?php endforeach; ?>
<?php endif; ?>

<?php if($class=='Worker'): ?>
    <?php foreach ($list as $item): ?>
        <?php if($email==$item['email']): ?>
            <h1>Уважаемый(ая) <?php echo $item['name'].' '.$item['patronymic']; ?>!</h1>
            <p>Вам начислена заработная плата в размере <?php echo $item['salary']; ?> долларов</p>
            <hr>
        <?php endif; ?>
    <?php endforeach;?>
<?php endif; ?>
   


