<?php
return [
    'adminEmail' => 'admin@example.com',
    'shortTextLimit' => 20,
    'shortWordLimit'=>5,
];
