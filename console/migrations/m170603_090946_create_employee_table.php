<?php

use yii\db\Migration;

/**
 * Handles the creation of table `employee`.
 */
class m170603_090946_create_employee_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('employee', [ 
            'first_name'=>$this->string(15)->notNull(),
            'last_name'=>$this->string(15)->notNull(),
            'middle_name'=>$this->string(15),
            'salary'=>$this->money(7,2),
            'email'=>$this->string(15)->notNull()->unique(),
            'dob'=>$this->date(),
            'start_date'=>$this->date()->notNull(),
            'city'=>$this->string(15),
            'position'=>$this->string(100)->notNull(),
            'id' => $this->integer(10)->unique()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('employee');
    }
}
