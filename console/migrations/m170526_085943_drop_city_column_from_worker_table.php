<?php

use yii\db\Migration;

/**
 * Handles dropping city from table `worker`.
 */
class m170526_085943_drop_city_column_from_worker_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('worker','city');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('worker','city', $this->string());
    }
}
