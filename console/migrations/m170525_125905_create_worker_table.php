<?php

use yii\db\Migration;

/**
 * Handles the creation of table `worker`.
 */
class m170525_125905_create_worker_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('worker', [
            'surname' => $this->string(),
            'name' => $this->string(),
            'patronymic' => $this->string(),
            'date_birth'=> $this->date(),
            'city' =>$this->string(),
            'start_date_work' => $this->date(),  //Дата начала работы
            'work_experience' => $this->integer(), //Стаж работы
            'position' => $this->string(),         //Должность
            'department_number' =>$this->integer(), //Номер отдела
            'id'=> $this->primaryKey(),
            'email'=> $this->string()->unique(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('worker');
    }
}
