<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comment`.
 */
class m170614_071208_create_comment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'name'=>$this->string()->notNull(),
            'email'=>$this->string(),
            'comment'=>$this->text()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('comment');
    }
}
