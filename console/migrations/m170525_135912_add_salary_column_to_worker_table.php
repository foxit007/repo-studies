<?php

use yii\db\Migration;

/**
 * Handles adding salary to table `worker`.
 */
class m170525_135912_add_salary_column_to_worker_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('worker','salary', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('worker','salary');
    }
}
