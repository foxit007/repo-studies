<?php

namespace common\components;

use Yii;

/**
 * Description of StringHelper
 * @author foxit
 */
class StringHelper {

    private $limit_char;
    private $limit_word;

    public function __construct() {
        $this->limit_char = Yii::$app->params['shortTextLimit'];
        $this->limit_word = Yii::$app->params['shortWordLimit'];
    }

    /**
     * @param string $string
     * @param integer $limit
     * @return string
     */
    public function getShortBySpace($string, $limit = null) {
        if ($limit === null) {
            $limit = $this->limit_char;
        }

        $string = strip_tags($string);

        if (mb_substr($string, $limit, 1) == "\x20") {
            return rtrim(mb_substr($string, 0, $limit), "!.,- ") . '...';
        } else {
            return rtrim(mb_substr($string, 0, mb_stripos($string, "\x20", $limit)), "!.,- ") . '...';
        }
    }

    /**
     * @param string $string
     * @param integer $limit
     * @return string
     */
    public function getShortByWord($string, $limit = null) {
        $pos = 0;
        if ($limit === null) {
            $limit = $this->limit_word;
        }

        $string = strip_tags($string);
        for ($i = 0; $i < $limit; $i++) {
            if (mb_strpos($string, "\x20", $pos)) {
                $pos = mb_strpos($string, "\x20", $pos) + 1;
            }
        }
        return rtrim(mb_substr($string, 0, $pos), "!,.- ") . '...';
    }

}
