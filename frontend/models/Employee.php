<?php

namespace frontend\models;
use yii\base\Model;
use Yii;
/**
 * @author foxit
 */
class Employee extends Model 
{
   
    const SCENARIO_EMPLOYEE_REGISTER='employee_register';
    const SCENARIO_EMPLOYEE_UPDATE='employee_update';
   
    public $firstName;
    public $lastName;
    public $middleName;
    public $salary;
    public $email;
    
    public $dob;
    public $start_date;
    public $city;
    public $position;
    public $id;
    
    public function scenarios() 
    {
        
        return[
            self::SCENARIO_EMPLOYEE_REGISTER => ['firstName','lastName','middleName','salary','email','dob','start_date','city','position','id'],
            self::SCENARIO_EMPLOYEE_UPDATE => ['firstName','lastName','middleName'],
        ];
    }
    
    public function rules()
    {
       
       return[
           [['firstName','lastName','email','start_date','position','id'], 'required'],
           [['firstName'], 'string', 'min'=>2],
           [['lastName'], 'string', 'min'=>3],
           [['email'], 'email'],
           [['middleName'], 'required', 'on'=>self::SCENARIO_EMPLOYEE_UPDATE],
           [['dob','start_date'], 'date', 'format' => 'php:Y-m-d'],
           [['city'], 'string'],
           ['id', 'string', 'min'=>10, 'max'=>10],
       ]; 
    }
    
    public function save() {
        
        $sql="INSERT INTO yii2advanced.employee (first_name, last_name, "
                . "middle_name, salary, email, dob, start_date, "
                . "city, position, id) "
                . "VALUES ("
                . "'{$this->firstName}',"
                . "'{$this->lastName}',"
                . "'{$this->middleName}',"
                . "'{$this->salary}',"
                . "'{$this->email}',"
                . "'{$this->dob}',"
                . "'{$this->start_date}',"
                . "'{$this->city}',"
                . "'{$this->position}',"
                . "'{$this->id}'"
                . ");";
        return Yii::$app->db->createCommand($sql)->execute();
    }
    
}
