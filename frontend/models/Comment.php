<?php

namespace frontend\models;
use yii\base\Model;
use Yii;

/**
 * @author foxit
 */
class Comment extends Model
{
    public $name;
    public $email;
    public $comment;
    
    public function rules()
    {
        return[
            [['name'], 'required'],
            [['email'],'email'],
            [['comment'], 'required'],
        ];      
    }
    
    public function saveComment()
    {
        $sql="INSERT INTO yii2advanced.comment (name, email, comment) "
                . "VALUES ("
                . "'{$this->name}',"
                . "'{$this->email}',"
                . "'{$this->comment}')";
        return Yii::$app->db->createCommand($sql)->execute();
    }
    
    public function getComment()
    {
        $sql='SELECT * FROM yii2advanced.comment';
        return Yii::$app->db->createCommand($sql)->queryAll();        
    }        
}
