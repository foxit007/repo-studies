<?php

namespace frontend\models;
use yii\base\Model;
use Yii;

/**
 * @author foxit
 */
class Client extends Model 
{
    
    public $width;
    public $height;
    public $camera;
    public $flap;
    public $flapTurn;
    public $color;
    public $sill;
    public $email;
    public $nameCustomer;
    
    public function rules()
    {
        return[
            [['width','height','camera','flap','flapTurn','color','sill','email','nameCustomer'],'required'],
            [['width'],'integer','min'=>70, 'max'=>210],
            [['height'],'integer', 'min'=>100, 'max'=>200],
            [['camera'],'in','range'=>[1,2,3]], 
            [['flap'],'integer','min'=>1],
            ['flap', 'compare','compareAttribute'=>'flapTurn','operator'=> '>'],         
            [['color'],'in','range'=>[1,2,3,4]],
            [['sill'],'in','range'=>[1,0]],
            [['email'],'email'],
            [['nameCustomer'],'string','min'=>3],
        ];
    }
    
    public function sendMail($orderClient=null)
    {
        return Yii::$app->mailer->compose('/mailer/adminMail', [
                           'orderClient'=>$orderClient, 
                        ])
                        ->setFrom(Yii::$app->params['adminEmail'])
                        ->setTo(Yii::$app->params['adminEmail'])
                        ->setSubject('Заказ окна')
                        ->send();
    }        
}
