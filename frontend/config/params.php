<?php
return [
    'maxNewsInList' => 4,
    'shortTextLimit' => 20,
    'shortWordLimit'=>5,
    'countEmployee'=>5,
    'maxViewComment'=>5,
];
