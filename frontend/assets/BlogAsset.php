<?php

namespace frontend\assets;
use yii\web\View;
use yii\web\AssetBundle;
/**
 * @author foxit
 */
class BlogAsset extends AssetBundle 
{
    public $css=[
        'blog/vendor/bootstrap/css/bootstrap.css',
        'blog/css/clean-blog.css',
        'blog/vendor/font-awesome/css/font-awesome.css',
        '//fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic',
        '//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800',
        
    ];
    
    public $js=[
        'blog/vendor/jquery/jquery.js',
        'blog/vendor/bootstrap/js/bootstrap.js',
        'blog/js/jqBootstrapValidation.js',
        'blog/js/contact_me.js',
        'blog/js/clean-blog.js',   
    ];
    
    public $jsOptions = [
        'position'=>View::POS_END,
    ];
    public $cssOptions = [
        'type' => 'text/css',
    ];
    
    public $depends=[
        
    ];
    
    
    
}
