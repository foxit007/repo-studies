<?php

namespace frontend\controllers;

use yii\web\Controller;
use frontend\models\Comment;
use Yii;
/**
 * @author foxit
 */
class BlogController extends Controller {
    
    public $layout='blog';
    
    public function actionHome()
    {
        print_r(Yii::$app); die;    
        return $this->render('home',[
            'img'=>'blog/img/home-bg.jpg',
        ]);
    }   
    
    public function actionAbout()
    {
     
        return $this->render('about');
    }  
    
    public function actionSamplePost()
    {
     
        return $this->render('samplePost');
    }   
    
    public function actionContact()
    {
     
        return $this->render('contact');
    } 
    
    public function actionComment()
    {
        $comment=new Comment();
        
        if(Yii::$app->request->isPost){
            $formComment=Yii::$app->request->post();
            $comment->attributes=$formComment;
            
            if($comment->validate()){
                //print_r($formComment);die;
                $comment->saveComment();
            }
        }
        return  $this->render('comment',[
            'commentData'=>$comment->getComment(),
            'comment'=>$comment,
        ]);
    }
}
