<?php

namespace frontend\controllers;
use yii\web\Controller;
use frontend\models\View;
//use Yii;

/**
 * Description of CountController
 * @author foxit
 */
class ViewController extends Controller {
    
    
    public function actionCount(){
        
        $count=View::getCountEntry();
       
        return $this->render('count',[
            'count' => $count,
        ]);
    }    
}
