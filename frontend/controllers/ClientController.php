<?php

namespace frontend\controllers;
use yii\web\Controller;
use frontend\models\Client;
use Yii;

/**
 * @author foxit
 */
class ClientController extends Controller 
{
    
    public function actionOrder()
    {
        $orderClient=new Client();
        $formData=Yii::$app->request->post();
        
        
        if(Yii::$app->request->isPost){
            
             $orderClient->attributes=$formData;
             
             if($orderClient->validate() && $orderClient->sendMail($formData)){
               Yii::$app->session->setFlash('success', 'Заказ оформлен и отправлен'); 
                
            }
        }
        
        return $this->render('order',[
            'orderClient'=>$orderClient,
        ]);
    }        
}
