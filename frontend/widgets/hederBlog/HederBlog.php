<?php

namespace frontend\widgets\hederBlog;
use yii\base\Widget;

/*
 * @author foxit
 */
class HederBlog extends Widget
{
 
    public $urlImgHeder=null;
    public $textH1=null;
    public $textSpan=null;
   
    
    public function run()
    {
        return $this->render('heder',[
            'urlImgHeder'=> $this->urlImgHeder,
            'textH1'=>      $this->textH1,
            'textSpan'=>    $this->textSpan,
        ]);
    }        
}
