<?php

namespace frontend\widgets\salaryList;
use Yii;
use yii\base\Widget;
use frontend\widgets\salaryList\models\Salary;

/**
 * @author foxit
 */
class SalaryList extends Widget 
{
 
    public $countLimit=null;
    
    public function run()
    {
       
        $max = Yii::$app->params['countEmployee'];
        $list = Salary::getSalaryMax($max);
        
        if($this->countLimit){
            $max=$this->countLimit;
        }
        $list=Salary::getSalaryMax($max);
        
        return $this->render('salary',[
            'list'=>$list,
        ]);
    }        
}
