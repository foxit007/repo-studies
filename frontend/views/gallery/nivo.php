<?php
/* @var $this yii\web\View */

use frontend\assets\NivoAsset;

NivoAsset::register($this);
$this->registerJsFile('@web/js/gallery/nivo.js', ['depends' => [
        NivoAsset::className(),
]]);
?>



<div id="slider" class="nivoSlider">     

    <img src="/files/photos/watch.jpg" alt="image">

    <img src="/files/photos/surf.jpg" alt="image">

    <img src="/files/photos/burger.jpg" alt="image">

    <img src="/files/photos/subway.jpg" alt="image">

    <img src="/files/photos/trees.jpg" alt="image">

    <img src="/files/photos/coffee.jpg" alt="image">

</div> 

<div id="htmlcaption" class="nivo-html-caption">     
    <strong>This</strong> is an example of a <em>HTML</em> caption with <a href="#">a link</a>. 
</div>