<?php /*@var $model frontend\models\Subscribe*/

if($orderClient->hasErrors()){
    echo '<pre>';
    print_r($orderClient->getErrors());
    echo '</pre>';
}
?>

<h1>Заказ окна</h1>
<form method="post">
    <p>Ширина окна:
    <input name="width" type="text" />
    </p>
    
    <p>Высота окна:
    <input name="height" type="text" />
    </p>
    
    <p>Количество камер:
        <input type="radio" name="camera" value="1"><label>1</label>
        <input type="radio" name="camera" value="2"><label>2</label>
        <input type="radio" name="camera" value="3"><label>3</label>
    </p>
    
    <p>Общее количество створок:
        <input name="flap" type="text" />
    </p>
    
    <p>Количество поворотных створок:
    <input name="flapTurn" type="text" />
    </p>
    
    <p>Цвет
    <select name="color">
        <option value="1">Белый</option>
        <option value="2">Черный</option>
        <option value="3">Красный</option>
        <option value="4">Желтый</option>
    </select>
    </p>
    
    <p>Наличие подоконника
        <input type="radio" name="sill" value="1" /><label>Да</label>
        <input type="radio" name="sill" value="0" /><label>Нет</label>
    </p>
    
    <p>Email
        <input name="email" type="email" />
    </p>
    
    <p>Имя закзчика:
        <input name="nameCustomer" type="text" />
    </p>
    
    <input type="submit" />
</form>
