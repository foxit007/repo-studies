<?php 
use yii\helpers\Html;
use frontend\widgets\hederBlog\HederBlog;
echo HederBlog::widget(['urlImgHeder'=>'/blog/img/about-bg.jpg','textH1'=>'About Me','textSpan'=>'This is what I do.']);
?>
<?php /*@var $model frontend\models\Subscribe*/

if($comment->hasErrors()){
    echo '<pre>';
    print_r($comment->getErrors());
    echo '</pre>';
}
?>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
            <div class="post-preview">
               <?php foreach ($commentData as $item): ?>
                <h5> <?php echo $item['name']; ?> </h5>
                <p> <?php echo Html::encode($item['comment']); ?> </p>
                <hr> 
               <?php endforeach; ?> 
            </div>
            <form method="post" id="comment">
                <input type="text" name="name" placeholder="Введите ваше имя" style="width: 500px" /><br>
                <input type="email" name="email" placeholder="Электронная почта" style="width: 500px" /><br>
                <textarea placeholder="Комментарий" name="comment" required form="comment"  style="width: 500px; height: 100px;"></textarea> <br>
                <input type="submit" value="Отправить" />
            </form>
        </div>
    </div>
</div>
