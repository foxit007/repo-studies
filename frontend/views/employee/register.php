<?php /*@var $model frontend\models\Subscribe*/

if($model->hasErrors()){
    echo '<pre>';
    print_r($model->getErrors());
    echo '</pre>';
}
?>
<h1>Welcome to our company!</h1>

<form method="post">
    <p>First name:</p>
    <input name="firstName" type="text"  autocomplete="on"/>
    <br><br>
    
    <p>Last name:</p>
    <input name="lastName" type="text" autocomplete="on" />
    <br><br>
    
    <p>Middle name:</p>
    <input name="middleName" type="text" autocomplete="on" />
    <br><br>
    
    <p>Salary:</p>
    <input name="salary" type="text" />
    <br><br>
    
    <p>Email:</p>
    <input name="email" type="text" />
    <br><br>
    
    <p>Date of birth</p>
    <input name="dob" type="text" />
    <br><br>
    
    <p>Start date:</p>
    <input name="start_date" type="text" />
    <br><br>
    
    <p>City:</p>
    <select name="city">
        <option value="1">Пятигорск</option>
        <option value="2">Ставрополь</option>
        <option value="3">Краснодар</option>
        <option value="4">Ростов на Дону</option>
    </select>
    <br><br>
    
    <p>Position:</p>
    <input name="position" type="text" />
    <br><br>
    
    <p>ID:</p>
    <input name="id" type="text" />
    <br><br>
    
    <input type="submit" />
</form>